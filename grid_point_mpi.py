import time as tm
import numpy as np
from math import sqrt, pi, cos, sin

#initializing MPI
from mpi4py import MPI

m = {}
m["H"] = 1.2
m["C"] = 1.7
m["N"] = 1.55
m["O"] = 1.52
m["F"] = 1.47
m["P"] = 1.8
m["S"] = 1.8
m["Cl"] = 1.75
m["Ar"] = 1.88
m["As"] = 1.85
m["Br"] = 1.85
m["Cd"] = 1.62
m["Cu"] = 1.4
m["Ga"] = 1.87
m["Au"] = 1.66
m["He"] = 1.4
m["In"] = 1.93
m["I"] = 1.98
m["Kr"] = 2.02
m["Pb"] = 2.02
m["Li"] = 1.82
m["Mg"] = 1.73
m["Hg"] = 1.70
m["Ne"] = 1.54
m["Ni"] = 1.64
m["Pd"] = 1.63
m["Pt"] = 1.8
m["K"] = 2.75
m["Se"] = 1.90
m["Si"] = 2.1
m["Ag"] = 1.9
m["Na"] = 2.27
m["Te"] = 2.06
m["Tl"] = 1.96
m["Sn"] = 2.17
m["U"] = 1.86
m["Xe"] = 2.16
m["Zn"] = 1.37

el_list = []
cor_list = []


def read_file(f, comm, rank, size):
    if rank == 0:
        with open(f,"r") as file:
            b = file.readlines()
            for line in b[2:]:
                el,x,y,z = line.split()
                el_list.append([el])
                cor_list.append([x,y,z])
        coordinates = np.array(cor_list,dtype=float)
        elements = np.array(el_list)
    else:
        coordinates = None
        elements = None

    coordinates = comm.bcast(coordinates, root=0)
    elements = comm.bcast(elements, root=0)
    
    return coordinates, elements


def dis(x_list,y_list):

    d = [x - y for x, y in zip(x_list, y_list)]

    return sqrt(d[0]**2 + d[1]**2 + d[2]**2)


def find_neighbours_mpi(coordinates, radii, comm, rank, size):

    neighbours_singlerank = []
    data = get_tasksize(len(coordinates), comm, rank, size)

    for i in data:
        for j in range(len(coordinates)):
            if i != j:
                ri = radii[i]
                rj = radii[j]

                rij = dis(coordinates[i],coordinates[j])
                if rij < (ri + rj):
                    neighbours_singlerank.append([i,j])

    if rank == 0:
        neighbours_tot = []
    else:
        neighbours_tot = None

    comm.barrier()
    neighbours_tot = comm.reduce(neighbours_singlerank, op=MPI.SUM, root=0)

    return neighbours_tot


def get_tasksize(data_size, comm, rank, size):
    
    if rank == 0:
        data = np.arange(data_size)

        num, res = divmod(data.size, size)
        portions = [num + 1 if p < res else num for p in range(size)]

        starts = [sum(portions[:p]) for p in range(size)]
        ends = [sum(portions[:p+1]) for p in range(size)]

        data = [data[starts[p]:ends[p]] for p in range(size)]
    else:
        data = None

    data = comm.scatter(data, root=0)

    return data


def generate_points(n_points):
    """
    Generates a list of equidistant points on a perfect sphere in cartesian format
    Parameters
    ----------
    n_points : int
        Number of points to generate
    """
    golden_angle = pi * (3 - sqrt(5))
    theta = [golden_angle * i for i in range(0, n_points)]
    z_vals = list(np.linspace(1 - 1.0 / n_points, 1.0 / n_points - 1, n_points))
    radius = [sqrt(1 - i * i) for i in z_vals]
    x_vals = [r * cos(t) for r, t in zip(radius, theta)]
    y_vals = [r * sin(t) for r, t in zip(radius, theta)]
    cartesian = list(zip(x_vals, y_vals, z_vals))

    return cartesian


def transform_spheres(cartesians, i, cor, radii):

    pot = []
    for j in range(len(cartesians)):
        s = [x * radii[i] for x in cartesians[j]]
        p = [x + y for x,y in zip(cor,s)]
        pot.append(p)

    return pot

def removing_points(neighbours_ele_ndx, i, coordinates, radii, pot):

    pot_survived = pot.copy()
    for pair in neighbours_ele_ndx:
        if i == pair[0]:
            for pt in pot:
                rj = radii[pair[1]]
                if dis(pt,coordinates[pair[1]]) <= rj and pt in pot_survived:
                    pot_survived.remove(pt)

    return pot_survived

def vdw_surface(coordinates, elements, comm, rank, size, density):
    
    if rank == 0:
        vdw_surface = []
    else:
        vdw_surface = None

    # mapping vdw radii to atoms:
    if rank == 0:
        radii = []
        for i in elements.T[0]:
            y = m.get(i)
            radii.append(y)
    else:
        radii = None

    radii = comm.bcast(radii, root=0)
    
    check_dot_pot = {}

    # finding all neighbour pairs:
    neighbours_ele_ndx = find_neighbours_mpi(coordinates, radii, comm, rank, size)
    neighbours_ele_ndx = comm.bcast(neighbours_ele_ndx, root=0)

    if rank == 0:
        vdw_surface_sc = []
    else:
        vdw_surface = None

    vdw_surface_singlerank = []
    data = get_tasksize(len(coordinates), comm, rank, size)

    for atom_num in data:

        # how many points on the sphere?
        n_points = round(density * 4.0 * pi * (radii[atom_num] ** 2))

        # generate sphere for this density if not already calculated:
        if n_points in check_dot_pot.keys():
            pass
        else:
            check_dot_pot[n_points] = generate_points(n_points)

        # get the cartesians of the sphere:
        dot_pot_cartesian = check_dot_pot[n_points]

        # adjust radius and spartial position of sphere:
        pot = transform_spheres(dot_pot_cartesian, atom_num, coordinates[atom_num], radii)

        # remove points within neighboured spheres:
        pot_survived = removing_points(neighbours_ele_ndx, atom_num, coordinates, radii, pot)

        vdw_surface_singlerank += pot_survived
    
    comm.barrier()
    vdw_surface_sc = comm.reduce(vdw_surface_singlerank, op=MPI.SUM, root=0)
    if rank == 0:
        vdw_surface += vdw_surface_sc

    return vdw_surface

 
def write_file(p):
    with open("test_mpi.xyz","w") as f:
        f.write(f"{len(p)}  \n\n")
        for i in p:
            f.write(f"  x   {i[0]}  {i[1]}  {i[2]}    \n")


def main(f, d):

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    if rank == 0:
        import cProfile
        import pstats
        import io
        import os
        pr = cProfile.Profile()
        pr.enable()
        timing_t0 = tm.time() 
    
    coordinates, elements = read_file(f, comm, rank, size)
    p = vdw_surface(coordinates, elements, comm, rank, size, density=d)
    if rank == 0:
        write_file(p)
    
        timing_tend = tm.time() - timing_t0
        print('Runtime: {:.2f} seconds'.format(timing_tend))

        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats(20)
        for line in s.getvalue().split(os.linesep):
            print(line)

if __name__=="__main__":
    import argparse 
    parser = argparse.ArgumentParser()
    parser.add_argument('-f',type=str,help='a name of file with extention.(ex: sample.xyz)')
    parser.add_argument('-d', type=int,default=5,help='Density: number of points per unit area of sphere')
    args = parser.parse_args()
    
    main(args.f, args.d)
